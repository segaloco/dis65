# dis65 - Pipeable MCS6500 Family Disassembler

## Rationale
Existing 6502 disassemblers present as multi-pass utilities capable of assigning labels, handling origin offsets, and other sophisticated features.  However, the nature of a multi-pass tool prevents then streaming data through the tool as a filter in a pipeline.

This disassembler does not support these more advanced features as would be seen in disassemblers like da65, but by keeping a narrow focus, this utility can be used as a filter.

## Usage
This disassembler processes a stream of binary data into 6500-family opcodes one byte at a time. Operations requiring additional bytes will read those additional bytes off the stream, if possible. Incomplete operations followed by EOF will be ignored. Unimplemented opcodes are treated as binary bytes and are emitted using the .byte directive commonly found in ca65 and other assemblers. Currently all binary bytes are presented as unsigned quantities.

## Testing
Included are two test cases, one for recognized opcodes and one for unrecognized bytes. In both cases the output of dis65 on the given binary file should match the corresponding text file. A third could potentially be created if support for undocumented opcodes is added.

## Future Direction
Indirect offsets should be signed.

Undocumented opcodes are not in my scope, but could potentially be added with a command-line option to enable them.

As time goes on, I may build further tools such as a label generator and a means to adjust indirect offsets against origins in the form of a second pass utility, but the concern of the dis65 tool will squarely be opcode interpretation into human-readable form as to remain pipe-able.
