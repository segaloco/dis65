/*
 * dis65 - pipeable MCS6500 family disassembler
 *
 * Copyright 2024 Matthew Gilmore
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

/* REF: MCS6500 Microcomputer Family Programming Manual - Appendix A (p. A-2) */
enum OPCODE_STRING_IDS {
	OPSTR_ADC,
	OPSTR_AND,
	OPSTR_ASL,
	OPSTR_BCC,
	OPSTR_BCS,
	OPSTR_BEQ,
	OPSTR_BIT,
	OPSTR_BMI,
	OPSTR_BNE,
	OPSTR_BPL,
	OPSTR_BRK,
	OPSTR_BVC,
	OPSTR_BVS,
	OPSTR_CLC,
	OPSTR_CLD,
	OPSTR_CLI,
	OPSTR_CLV,
	OPSTR_CMP,
	OPSTR_CPX,
	OPSTR_CPY,
	OPSTR_DEC,
	OPSTR_DEX,
	OPSTR_DEY,
	OPSTR_EOR,
	OPSTR_INC,
	OPSTR_INX,
	OPSTR_INY,
	OPSTR_JMP,
	OPSTR_JSR,
	OPSTR_LDA,
	OPSTR_LDX,
	OPSTR_LDY,
	OPSTR_LSR,
	OPSTR_NOP,
	OPSTR_ORA,
	OPSTR_PHA,
	OPSTR_PHP,
	OPSTR_PLA,
	OPSTR_PLP,
	OPSTR_ROL,
	OPSTR_ROR,
	OPSTR_RTI,
	OPSTR_RTS,
	OPSTR_SBC,
	OPSTR_SEC,
	OPSTR_SED,
	OPSTR_SEI,
	OPSTR_STA,
	OPSTR_STX,
	OPSTR_STY,
	OPSTR_TAX,
	OPSTR_TAY,
	OPSTR_TSX,
	OPSTR_TXA,
	OPSTR_TXS,
	OPSTR_TYA
};

/* REF: MCS6500 Microcomputer Family Programming Manual - Appendix A (p. A-2) */
const char *OPCODE_STRINGS[] = {
	"adc",
	"and",
	"asl",
	"bcc",
	"bcs",
	"beq",
	"bit",
	"bmi",
	"bne",
	"bpl",
	"brk",
	"bvc",
	"bvs",
	"clc",
	"cld",
	"cli",
	"clv",
	"cmp",
	"cpx",
	"cpy",
	"dec",
	"dex",
	"dey",
	"eor",
	"inc",
	"inx",
	"iny",
	"jmp",
	"jsr",
	"lda",
	"ldx",
	"ldy",
	"lsr",
	"nop",
	"ora",
	"pha",
	"php",
	"pla",
	"plp",
	"rol",
	"ror",
	"rti",
	"rts",
	"sbc",
	"sec",
	"sed",
	"sei",
	"sta",
	"stx",
	"sty",
	"tax",
	"tay",
	"tsx",
	"txa",
	"txs",
	"tya"
};

/* REF: MCS6500 Microcomputer Family Programming Manual - Appendix D (pp. D-2 - D-5) */
enum OPCODES_6502 {
	OP_BRK		= 0x00,
	OP_ORA_IND_X	= 0x01,
	OP_ORA_ZP	= 0x05,
	OP_ASL_ZP	= 0x06,
	OP_PHP		= 0x08,
	OP_ORA_IMM	= 0x09,
	OP_ASL_A	= 0x0A,
	OP_ORA_P	= 0x0D,
	OP_ASL_P	= 0x0E,
	OP_BPL		= 0x10,
	OP_ORA_IND_Y	= 0x11,
	OP_ORA_ZP_X	= 0x15,
	OP_ASL_ZP_X	= 0x16,
	OP_CLC		= 0x18,
	OP_ORA_P_Y	= 0x19,
	OP_ORA_P_X	= 0x1D,
	OP_ASL_P_X	= 0x1E,
	OP_JSR		= 0x20,
	OP_AND_IND_X	= 0x21,
	OP_BIT_ZP	= 0x24,
	OP_AND_ZP	= 0x25,
	OP_ROL_ZP	= 0x26,
	OP_PLP		= 0x28,
	OP_AND_IMM	= 0x29,
	OP_ROL_A	= 0x2A,
	OP_BIT_P	= 0x2C,
	OP_AND_P	= 0x2D,
	OP_ROL_P	= 0x2E,
	OP_BMI		= 0x30,
	OP_AND_IND_Y	= 0x31,
	OP_AND_ZP_X	= 0x35,
	OP_ROL_ZP_X	= 0x36,
	OP_SEC		= 0x38,
	OP_AND_P_Y	= 0x39,
	OP_AND_P_X	= 0x3D,
	OP_ROL_P_X	= 0x3E,
	OP_RTI		= 0x40,
	OP_EOR_IND_X	= 0x41,
	OP_EOR_ZP	= 0x45,
	OP_LSR_ZP	= 0x46,
	OP_PHA		= 0x48,
	OP_EOR_IMM	= 0x49,
	OP_LSR_A	= 0x4A,
	OP_JMP_P	= 0x4C,
	OP_EOR_P	= 0x4D,
	OP_LSR_P	= 0x4E,
	OP_BVC		= 0x50,
	OP_EOR_IND_Y	= 0x51,
	OP_EOR_ZP_X	= 0x55,
	OP_LSR_ZP_X	= 0x56,
	OP_CLI		= 0x58,
	OP_EOR_P_Y	= 0x59,
	OP_EOR_P_X	= 0x5D,
	OP_LSR_P_X	= 0x5E,
	OP_RTS		= 0x60,
	OP_ADC_IND_X	= 0x61,
	OP_ADC_ZP	= 0x65,
	OP_ROR_ZP	= 0x66,
	OP_PLA		= 0x68,
	OP_ADC_IMM	= 0x69,
	OP_ROR_A	= 0x6A,
	OP_JMP_IND	= 0x6C,
	OP_ADC_P	= 0x6D,
	OP_ROR_P	= 0x6E,
	OP_BVS		= 0x70,
	OP_ADC_IND_Y	= 0x71,
	OP_ADC_ZP_X	= 0x75,
	OP_ROR_ZP_X	= 0x76,
	OP_SEI		= 0x78,
	OP_ADC_P_Y	= 0x79,
	OP_ADC_P_X	= 0x7D,
	OP_ROR_P_X	= 0x7E,
	OP_STA_IND_X	= 0x81,
	OP_STY_ZP	= 0x84,
	OP_STA_ZP	= 0x85,
	OP_STX_ZP	= 0x86,
	OP_DEY		= 0x88,
	OP_TXA		= 0x8A,
	OP_STY_P	= 0x8C,
	OP_STA_P	= 0x8D,
	OP_STX_P	= 0x8E,
	OP_BCC		= 0x90,
	OP_STA_IND_Y	= 0x91,
	OP_STY_ZP_X	= 0x94,
	OP_STA_ZP_X	= 0x95,
	OP_STX_ZP_Y	= 0x96,
	OP_TYA		= 0x98,
	OP_STA_P_Y	= 0x99,
	OP_TXS		= 0x9A,
	OP_STA_P_X	= 0x9D,
	OP_LDY_IMM	= 0xA0,
	OP_LDA_IND_X	= 0xA1,
	OP_LDX_IMM	= 0xA2,
	OP_LDY_ZP	= 0xA4,
	OP_LDA_ZP	= 0xA5,
	OP_LDX_ZP	= 0xA6,
	OP_TAY		= 0xA8,
	OP_LDA_IMM	= 0xA9,
	OP_TAX		= 0xAA,
	OP_LDY_P	= 0xAC,
	OP_LDA_P	= 0xAD,
	OP_LDX_P	= 0xAE,
	OP_BCS		= 0xB0,
	OP_LDA_IND_Y	= 0xB1,
	OP_LDY_ZP_X	= 0xB4,
	OP_LDA_ZP_X	= 0xB5,
	OP_LDX_ZP_Y	= 0xB6,
	OP_CLV		= 0xB8,
	OP_LDA_P_Y	= 0xB9,
	OP_TSX		= 0xBA,
	OP_LDY_P_X	= 0xBC,
	OP_LDA_P_X	= 0xBD,
	OP_LDX_P_Y	= 0xBE,
	OP_CPY_IMM	= 0xC0,
	OP_CMP_IND_X	= 0xC1,
	OP_CPY_ZP	= 0xC4,
	OP_CMP_ZP	= 0xC5,
	OP_DEC_ZP	= 0xC6,
	OP_INY		= 0xC8,
	OP_CMP_IMM	= 0xC9,
	OP_DEX		= 0xCA,
	OP_CPY_P	= 0xCC,
	OP_CMP_P	= 0xCD,
	OP_DEC_P	= 0xCE,
	OP_BNE		= 0xD0,
	OP_CMP_IND_Y	= 0xD1,
	OP_CMP_ZP_X	= 0xD5,
	OP_DEC_ZP_X	= 0xD6,
	OP_CLD		= 0xD8,
	OP_CMP_P_Y	= 0xD9,
	OP_CMP_P_X	= 0xDD,
	OP_DEC_P_X	= 0xDE,
	OP_CPX_IMM	= 0xE0,
	OP_SBC_IND_X	= 0xE1,
	OP_CPX_ZP	= 0xE4,
	OP_SBC_ZP	= 0xE5,
	OP_INC_ZP	= 0xE6,
	OP_INX		= 0xE8,
	OP_SBC_IMM	= 0xE9,
	OP_NOP		= 0xEA,
	OP_CPX_P	= 0xEC,
	OP_SBC_P	= 0xED,
	OP_INC_P	= 0xEE,
	OP_BEQ		= 0xF0,
	OP_SBC_IND_Y	= 0xF1,
	OP_SBC_ZP_X	= 0xF5,
	OP_INC_ZP_X	= 0xF6,
	OP_SED		= 0xF8,
	OP_SBC_P_Y	= 0xF9,
	OP_SBC_P_X	= 0xFD,
	OP_INC_P_X	= 0xFE 
};

/* REF: MCS6500 Microcomputer Family Programming Manual - 4.1 Branching (p. 37) */
void printbr(const char *opcode)
{
	int c;
	signed char d;

	if ((c = getchar()) != EOF) {
		d = c;

		printf("\t%s\t%s$%02x\n", opcode, d < 0 ? "<-" : "",  d < 0 ? -d : d);
	}
}

/* REF: MCS6500 Microcomputer Family Programming Manual - 5.3 Implied Addressing (p. 57) */
void printa(const char *opcode)
{
	printf("\t%s\ta\n", opcode);
}

/* REF: MCS6500 Microcomputer Family Programming Manual - 5.3 Implied Addressing (p. 57) */
void printop(const char *opcode)
{
	printf("\t%s\n", opcode);
}

/* REF: MCS6500 Microcomputer Family Programming Manual - 5.4 Immediate Addressing (p. 59) */
void printimm(const char *opcode)
{
	int c;

	if ((c = getchar()) != EOF)
		printf("\t%s\t#$%02x\n", opcode, (unsigned char) c);
}

/* REF: MCS6500 Microcomputer Family Programming Manual - 5.5 Absolute Addressing (p. 59) */
void printp(const char *opcode)
{
	int c_l, c_h;

	if ((c_l = getchar()) != EOF && (c_h = getchar()) != EOF)
		printf("\t%s\t$%02x%02x\n", opcode, (unsigned char) c_h, (unsigned char) c_l);
}

/* REF: MCS6500 Microcomputer Family Programming Manual - 5.6 Zero Page Addressing (p. 61) */
void printzp(const char *opcode)
{
	int c;

	if ((c = getchar()) != EOF)
		printf("\t%s\t$%02x\n", opcode, (unsigned char) c);
}

/* REF: MCS6500 Microcomputer Family Programming Manual - 6.1 Absolute Indexed (p. 79) */
void printpx(const char *opcode)
{
	int c_l, c_h;

	if ((c_l = getchar()) != EOF && (c_h = getchar()) != EOF)
		printf("\t%s\t$%02x%02x, x\n", opcode, (unsigned char) c_h, (unsigned char) c_l);
}

/* REF: MCS6500 Microcomputer Family Programming Manual - 6.1 Absolute Indexed (p. 79) */
void printpy(const char *opcode)
{
	int c_l, c_h;

	if ((c_l = getchar()) != EOF && (c_h = getchar()) != EOF)
		printf("\t%s\t$%02x%02x, y\n", opcode, (unsigned char) c_h, (unsigned char) c_l);
}

/* REF: MCS6500 Microcomputer Family Programming Manual - 6.2 Zero Page Indexed (p. 81) */
void printzpx(const char *opcode)
{
	int c;

	if ((c = getchar()) != EOF)
		printf("\t%s\t$%02x, x\n", opcode, (unsigned char) c);
}

/* REF: MCS6500 Microcomputer Family Programming Manual - 6.2 Zero Page Indexed (p. 81) */
void printzpy(const char *opcode)
{
	int c;

	if ((c = getchar()) != EOF)
		printf("\t%s\t$%02x, y\n", opcode, (unsigned char) c);
}

/* REF: MCS6500 Microcomputer Family Programming Manual - 6.3 Indirect Addressing (p. 83) */
void printind(const char *opcode)
{
	int c;

	if ((c = getchar()) != EOF)
		printf("\t%s\t($%02x)\n", opcode, (unsigned char) c);
}

/* REF: MCS6500 Microcomputer Family Programming Manual - 6.4 Indexed Indirect Addressing (p. 85) */
void printindx(const char *opcode)
{
	int c;

	if ((c = getchar()) != EOF)
		printf("\t%s\t($%02x, x)\n", opcode, (unsigned char) c);
}

void printindy(const char *opcode)
{
	int c;

	if ((c = getchar()) != EOF)
		printf("\t%s\t($%02x), y\n", opcode, (unsigned char) c);
}

/* REF: ca65 Users Guide - 11.10 .BYT/.BYTE (https://cc65.github.io/doc/ca65.html#ss11.10) */
void printb(unsigned char c)
{
	printf("\t.byte\t$%02x\n", c);
}

int main(int argc, char *argv[])
{
	int c;
	const char *opcode;

	if (argc > 3) {
		fprintf(stderr, "Usage: %s [ path ] [ output ]\n", argv[0]);
		return EXIT_FAILURE;
	}
	
	if (argc >= 2 && freopen(argv[1], "rb", stdin) == NULL) {
		fprintf(stderr, "Error: Could not open input file.\n");
		return EXIT_FAILURE;
	}

	if (argc == 3 && freopen(argv[2], "wb", stdout) == NULL) {
		fprintf(stderr, "Error: Could not open output file.\n");
		return EXIT_FAILURE;
	}

	while ((c = getchar()) != EOF) {
		switch (c) {
		case OP_ADC_IMM:
			printimm(OPCODE_STRINGS[OPSTR_ADC]);
			break;
		case OP_ADC_ZP:
			printzp(OPCODE_STRINGS[OPSTR_ADC]);
			break;
		case OP_ADC_ZP_X:
			printzpx(OPCODE_STRINGS[OPSTR_ADC]);
			break;
		case OP_ADC_P:
			printp(OPCODE_STRINGS[OPSTR_ADC]);
			break;
		case OP_ADC_P_X:
			printpx(OPCODE_STRINGS[OPSTR_ADC]);
			break;
		case OP_ADC_P_Y:
			printpy(OPCODE_STRINGS[OPSTR_ADC]);
			break;
		case OP_ADC_IND_X:
			printindx(OPCODE_STRINGS[OPSTR_ADC]);
			break;
		case OP_ADC_IND_Y:
			printindy(OPCODE_STRINGS[OPSTR_ADC]);
			break;
		case OP_AND_IMM:
			printimm(OPCODE_STRINGS[OPSTR_AND]);
			break;
		case OP_AND_ZP:
			printzp(OPCODE_STRINGS[OPSTR_AND]);
			break;
		case OP_AND_ZP_X:
			printzpx(OPCODE_STRINGS[OPSTR_AND]);
			break;
		case OP_AND_P:
			printp(OPCODE_STRINGS[OPSTR_AND]);
			break;
		case OP_AND_P_X:
			printpx(OPCODE_STRINGS[OPSTR_AND]);
			break;
		case OP_AND_P_Y:
			printpy(OPCODE_STRINGS[OPSTR_AND]);
			break;
		case OP_AND_IND_X:
			printindx(OPCODE_STRINGS[OPSTR_AND]);
			break;
		case OP_AND_IND_Y:
			printindy(OPCODE_STRINGS[OPSTR_AND]);
			break;
		case OP_ASL_A:
			printa(OPCODE_STRINGS[OPSTR_ASL]);
			break;
		case OP_ASL_ZP:
			printzp(OPCODE_STRINGS[OPSTR_ASL]);
			break;
		case OP_ASL_ZP_X:
			printzpx(OPCODE_STRINGS[OPSTR_ASL]);
			break;
		case OP_ASL_P:
			printp(OPCODE_STRINGS[OPSTR_ASL]);
			break;
		case OP_ASL_P_X:
			printpx(OPCODE_STRINGS[OPSTR_ASL]);
			break;
		case OP_BCC:
			printbr(OPCODE_STRINGS[OPSTR_BCC]);
			break;
		case OP_BCS:
			printbr(OPCODE_STRINGS[OPSTR_BCS]);
			break;
		case OP_BEQ:
			printbr(OPCODE_STRINGS[OPSTR_BEQ]);
			break;
		case OP_BIT_ZP:
			printzp(OPCODE_STRINGS[OPSTR_BIT]);
			break;
		case OP_BIT_P:
			printp(OPCODE_STRINGS[OPSTR_BIT]);
			break;
		case OP_BMI:
			printbr(OPCODE_STRINGS[OPSTR_BMI]);
			break;
		case OP_BNE:
			printbr(OPCODE_STRINGS[OPSTR_BNE]);
			break;
		case OP_BPL:
			printbr(OPCODE_STRINGS[OPSTR_BPL]);
			break;
		case OP_BRK:
			printop(OPCODE_STRINGS[OPSTR_BRK]);
			break;
		case OP_BVC:
			printbr(OPCODE_STRINGS[OPSTR_BVC]);
			break;
		case OP_BVS:
			printbr(OPCODE_STRINGS[OPSTR_BVS]);
			break;
		case OP_CLC:
			printop(OPCODE_STRINGS[OPSTR_CLC]);
			break;
		case OP_CLD:
			printop(OPCODE_STRINGS[OPSTR_CLD]);
			break;
		case OP_CLI:
			printop(OPCODE_STRINGS[OPSTR_CLI]);
			break;
		case OP_CLV:
			printop(OPCODE_STRINGS[OPSTR_CLV]);
			break;
		case OP_CMP_IMM:
			printimm(OPCODE_STRINGS[OPSTR_CMP]);
			break;
		case OP_CMP_ZP:
			printzp(OPCODE_STRINGS[OPSTR_CMP]);
			break;
		case OP_CMP_ZP_X:
			printzpx(OPCODE_STRINGS[OPSTR_CMP]);
			break;
		case OP_CMP_P:
			printp(OPCODE_STRINGS[OPSTR_CMP]);
			break;
		case OP_CMP_P_X:
			printpx(OPCODE_STRINGS[OPSTR_CMP]);
			break;
		case OP_CMP_P_Y:
			printpy(OPCODE_STRINGS[OPSTR_CMP]);
			break;
		case OP_CMP_IND_X:
			printindx(OPCODE_STRINGS[OPSTR_CMP]);
			break;
		case OP_CMP_IND_Y:
			printindy(OPCODE_STRINGS[OPSTR_CMP]);
			break;
		case OP_CPX_IMM:
			printimm(OPCODE_STRINGS[OPSTR_CPX]);
			break;
		case OP_CPX_ZP:
			printzp(OPCODE_STRINGS[OPSTR_CPX]);
			break;
		case OP_CPX_P:
			printp(OPCODE_STRINGS[OPSTR_CPX]);
			break;
		case OP_CPY_IMM:
			printimm(OPCODE_STRINGS[OPSTR_CPY]);
			break;
		case OP_CPY_ZP:
			printzp(OPCODE_STRINGS[OPSTR_CPY]);
			break;
		case OP_CPY_P:
			printp(OPCODE_STRINGS[OPSTR_CPY]);
			break;
		case OP_DEC_ZP:
			printzp(OPCODE_STRINGS[OPSTR_DEC]);
			break;
		case OP_DEC_ZP_X:
			printzpx(OPCODE_STRINGS[OPSTR_DEC]);
			break;
		case OP_DEC_P:
			printp(OPCODE_STRINGS[OPSTR_DEC]);
			break;
		case OP_DEC_P_X:
			printpx(OPCODE_STRINGS[OPSTR_DEC]);
			break;
		case OP_DEX:
			printop(OPCODE_STRINGS[OPSTR_DEX]);
			break;
		case OP_DEY:
			printop(OPCODE_STRINGS[OPSTR_DEY]);
			break;
		case OP_EOR_IMM:
			printimm(OPCODE_STRINGS[OPSTR_EOR]);
			break;
		case OP_EOR_ZP:
			printzp(OPCODE_STRINGS[OPSTR_EOR]);
			break;
		case OP_EOR_ZP_X:
			printzpx(OPCODE_STRINGS[OPSTR_EOR]);
			break;
		case OP_EOR_P:
			printp(OPCODE_STRINGS[OPSTR_EOR]);
			break;
		case OP_EOR_P_X:
			printpx(OPCODE_STRINGS[OPSTR_EOR]);
			break;
		case OP_EOR_P_Y:
			printpy(OPCODE_STRINGS[OPSTR_EOR]);
			break;
		case OP_EOR_IND_X:
			printindx(OPCODE_STRINGS[OPSTR_EOR]);
			break;
		case OP_EOR_IND_Y:
			printindy(OPCODE_STRINGS[OPSTR_EOR]);
			break;
		case OP_INC_ZP:
			printzp(OPCODE_STRINGS[OPSTR_INC]);
			break;
		case OP_INC_ZP_X:
			printzpx(OPCODE_STRINGS[OPSTR_INC]);
			break;
		case OP_INC_P:
			printp(OPCODE_STRINGS[OPSTR_INC]);
			break;
		case OP_INC_P_X:
			printpx(OPCODE_STRINGS[OPSTR_INC]);
			break;
		case OP_INX:
			printop(OPCODE_STRINGS[OPSTR_INX]);
			break;
		case OP_INY:
			printop(OPCODE_STRINGS[OPSTR_INY]);
			break;
		case OP_JMP_P:
			printp(OPCODE_STRINGS[OPSTR_JMP]);
			break;
		case OP_JMP_IND:
			printind(OPCODE_STRINGS[OPSTR_JMP]);
			break;
		case OP_JSR:
			printp(OPCODE_STRINGS[OPSTR_JSR]);
			break;
		case OP_LDA_IMM:
			printimm(OPCODE_STRINGS[OPSTR_LDA]);
			break;
		case OP_LDA_ZP:
			printzp(OPCODE_STRINGS[OPSTR_LDA]);
			break;
		case OP_LDA_ZP_X:
			printzpx(OPCODE_STRINGS[OPSTR_LDA]);
			break;
		case OP_LDA_P:
			printp(OPCODE_STRINGS[OPSTR_LDA]);
			break;
		case OP_LDA_P_X:
			printpx(OPCODE_STRINGS[OPSTR_LDA]);
			break;
		case OP_LDA_P_Y:
			printpy(OPCODE_STRINGS[OPSTR_LDA]);
			break;
		case OP_LDA_IND_X:
			printindx(OPCODE_STRINGS[OPSTR_LDA]);
			break;
		case OP_LDA_IND_Y:
			printindy(OPCODE_STRINGS[OPSTR_LDA]);
			break;
		case OP_LDX_IMM:
			printimm(OPCODE_STRINGS[OPSTR_LDX]);
			break;
		case OP_LDX_ZP:
			printzp(OPCODE_STRINGS[OPSTR_LDX]);
			break;
		case OP_LDX_ZP_Y:
			printzpy(OPCODE_STRINGS[OPSTR_LDX]);
			break;
		case OP_LDX_P:
			printp(OPCODE_STRINGS[OPSTR_LDX]);
			break;
		case OP_LDX_P_Y:
			printpy(OPCODE_STRINGS[OPSTR_LDX]);
			break;
		case OP_LDY_IMM:
			printimm(OPCODE_STRINGS[OPSTR_LDY]);
			break;
		case OP_LDY_ZP:
			printzp(OPCODE_STRINGS[OPSTR_LDY]);
			break;
		case OP_LDY_ZP_X:
			printzpx(OPCODE_STRINGS[OPSTR_LDY]);
			break;
		case OP_LDY_P:
			printp(OPCODE_STRINGS[OPSTR_LDY]);
			break;
		case OP_LDY_P_X:
			printpx(OPCODE_STRINGS[OPSTR_LDY]);
			break;
		case OP_LSR_A:
			printa(OPCODE_STRINGS[OPSTR_LSR]);
			break;
		case OP_LSR_ZP:
			printzp(OPCODE_STRINGS[OPSTR_LSR]);
			break;
		case OP_LSR_ZP_X:
			printzpx(OPCODE_STRINGS[OPSTR_LSR]);
			break;
		case OP_LSR_P:
			printp(OPCODE_STRINGS[OPSTR_LSR]);
			break;
		case OP_LSR_P_X:
			printpx(OPCODE_STRINGS[OPSTR_LSR]);
			break;
		case OP_NOP:
			printop(OPCODE_STRINGS[OPSTR_NOP]);
			break;
		case OP_ORA_IMM:
			printimm(OPCODE_STRINGS[OPSTR_ORA]);
			break;
		case OP_ORA_ZP:
			printzp(OPCODE_STRINGS[OPSTR_ORA]);
			break;
		case OP_ORA_ZP_X:
			printzpx(OPCODE_STRINGS[OPSTR_ORA]);
			break;
		case OP_ORA_P:
			printp(OPCODE_STRINGS[OPSTR_ORA]);
			break;
		case OP_ORA_P_X:
			printpx(OPCODE_STRINGS[OPSTR_ORA]);
			break;
		case OP_ORA_P_Y:
			printpy(OPCODE_STRINGS[OPSTR_ORA]);
			break;
		case OP_ORA_IND_X:
			printindx(OPCODE_STRINGS[OPSTR_ORA]);
			break;
		case OP_ORA_IND_Y:
			printindy(OPCODE_STRINGS[OPSTR_ORA]);
			break;
		case OP_PHA:
			printop(OPCODE_STRINGS[OPSTR_PHA]);
			break;
		case OP_PHP:
			printop(OPCODE_STRINGS[OPSTR_PHP]);
			break;
		case OP_PLA:
			printop(OPCODE_STRINGS[OPSTR_PLA]);
			break;
		case OP_PLP:
			printop(OPCODE_STRINGS[OPSTR_PLP]);
			break;
		case OP_ROL_A:
			printa(OPCODE_STRINGS[OPSTR_ROL]);
			break;
		case OP_ROL_ZP:
			printzp(OPCODE_STRINGS[OPSTR_ROL]);
			break;
		case OP_ROL_ZP_X:
			printzpx(OPCODE_STRINGS[OPSTR_ROL]);
			break;
		case OP_ROL_P:
			printp(OPCODE_STRINGS[OPSTR_ROL]);
			break;
		case OP_ROL_P_X:
			printpx(OPCODE_STRINGS[OPSTR_ROL]);
			break;
		case OP_ROR_A:
			printa(OPCODE_STRINGS[OPSTR_ROR]);
			break;
		case OP_ROR_ZP:
			printzp(OPCODE_STRINGS[OPSTR_ROR]);
			break;
		case OP_ROR_ZP_X:
			printzpx(OPCODE_STRINGS[OPSTR_ROR]);
			break;
		case OP_ROR_P:
			printp(OPCODE_STRINGS[OPSTR_ROR]);
			break;
		case OP_ROR_P_X:
			printpx(OPCODE_STRINGS[OPSTR_ROR]);
			break;
		case OP_RTI:
			printop(OPCODE_STRINGS[OPSTR_RTI]);
			break;
		case OP_RTS:
			printop(OPCODE_STRINGS[OPSTR_RTS]);
			break;
		case OP_SBC_IMM:
			printimm(OPCODE_STRINGS[OPSTR_SBC]);
			break;
		case OP_SBC_ZP:
			printzp(OPCODE_STRINGS[OPSTR_SBC]);
			break;
		case OP_SBC_ZP_X:
			printzpx(OPCODE_STRINGS[OPSTR_SBC]);
			break;
		case OP_SBC_P:
			printp(OPCODE_STRINGS[OPSTR_SBC]);
			break;
		case OP_SBC_P_X:
			printpx(OPCODE_STRINGS[OPSTR_SBC]);
			break;
		case OP_SBC_P_Y:
			printpy(OPCODE_STRINGS[OPSTR_SBC]);
			break;
		case OP_SBC_IND_X:
			printindx(OPCODE_STRINGS[OPSTR_SBC]);
			break;
		case OP_SBC_IND_Y:
			printindy(OPCODE_STRINGS[OPSTR_SBC]);
			break;
		case OP_SEC:
			printop(OPCODE_STRINGS[OPSTR_SEC]);
			break;
		case OP_SED:
			printop(OPCODE_STRINGS[OPSTR_SED]);
			break;
		case OP_SEI:
			printop(OPCODE_STRINGS[OPSTR_SEI]);
			break;
		case OP_STA_ZP:
			printzp(OPCODE_STRINGS[OPSTR_STA]);
			break;
		case OP_STA_ZP_X:
			printzpx(OPCODE_STRINGS[OPSTR_STA]);
			break;
		case OP_STA_P:
			printp(OPCODE_STRINGS[OPSTR_STA]);
			break;
		case OP_STA_P_X:
			printpx(OPCODE_STRINGS[OPSTR_STA]);
			break;
		case OP_STA_P_Y:
			printpy(OPCODE_STRINGS[OPSTR_STA]);
			break;
		case OP_STA_IND_X:
			printindx(OPCODE_STRINGS[OPSTR_STA]);
			break;
		case OP_STA_IND_Y:
			printindy(OPCODE_STRINGS[OPSTR_STA]);
			break;
		case OP_STX_ZP:
			printzp(OPCODE_STRINGS[OPSTR_STX]);
			break;
		case OP_STX_ZP_Y:
			printzpy(OPCODE_STRINGS[OPSTR_STX]);
			break;
		case OP_STX_P:
			printp(OPCODE_STRINGS[OPSTR_STX]);
			break;
		case OP_STY_ZP:
			printzp(OPCODE_STRINGS[OPSTR_STY]);
			break;
		case OP_STY_ZP_X:
			printzpx(OPCODE_STRINGS[OPSTR_STY]);
			break;
		case OP_STY_P:
			printp(OPCODE_STRINGS[OPSTR_STY]);
			break;
		case OP_TAX:
			printop(OPCODE_STRINGS[OPSTR_TAX]);
			break;
		case OP_TAY:
			printop(OPCODE_STRINGS[OPSTR_TAY]);
			break;
		case OP_TYA:
			printop(OPCODE_STRINGS[OPSTR_TYA]);
			break;
		case OP_TSX:
			printop(OPCODE_STRINGS[OPSTR_TSX]);
			break;
		case OP_TXA:
			printop(OPCODE_STRINGS[OPSTR_TXA]);
			break;
		case OP_TXS:
			printop(OPCODE_STRINGS[OPSTR_TXS]);
			break;
		default:
			printb(c);
			break;
		}
	}

	return EXIT_SUCCESS;
}
