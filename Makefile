BIN	=	dis65

MAIN	=	main.o

OBJS	=	$(MAIN)

$(BIN): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $< $(LIBS)

test: $(BIN)
	@./$(BIN) <tests/ops.bin | cmp - tests/ops.txt 2>/dev/null || echo "opcode test failed"
	@./$(BIN) <tests/undef.bin | cmp - tests/undef.txt 2>/dev/null || echo "undefined test failed"
	@./$(BIN) <tests/single_digit.bin | cmp - tests/single_digit.txt 2>/dev/null || echo "single digit test failed"

clean:
	rm -rf $(BIN) $(OBJS)
